package main

import (
	"fmt"
	"sync"
)

func main() {
	wg := sync.WaitGroup{}
	mu := sync.Mutex{}

	counter := 0
	for i := 0; i < 1000; i++ {
		wg.Add(1)

		go func() {
			mu.Lock()
			counter++
			mu.Unlock()

			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(counter)
}
