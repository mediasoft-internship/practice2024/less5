package main

import (
	"fmt"
	"sync"
)

func main() {
	wg := sync.WaitGroup{}

	counter := 0
	for i := 0; i < 1000; i++ {
		wg.Add(1)

		go func() {
			counter++
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(counter)
}
