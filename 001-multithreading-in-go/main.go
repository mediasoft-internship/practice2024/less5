package main

import (
	"fmt"
	"time"
)

func task(i int) {
	time.Sleep(2 * time.Second)
	fmt.Printf("success %d\n", i)
}

func main() {
	tNow := time.Now()

	// use go to declare goroutine
	task(1)
	task(2)

	fmt.Printf("execution time: %v\n", time.Since(tNow))
}
