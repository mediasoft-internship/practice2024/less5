package main

import (
	"fmt"
	"math/rand"
	"sync"
)

func main() {
	wg := sync.WaitGroup{}
	mp := sync.Map{}

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			k := rand.Intn(10) + 1  // from 1 to 10
			v := rand.Intn(100) + 1 // from 1 to 100
			mp.Store(k, v)
		}()
	}

	wg.Wait()

	mp.Range(func(k, v interface{}) bool {
		fmt.Printf("key: [%d] value: [%d]\n", k, v)
		return true
	})
}
