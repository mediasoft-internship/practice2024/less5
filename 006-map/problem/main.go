package main

import (
	"math/rand"
	"sync"
)

func main() {
	wg := sync.WaitGroup{}
	mp := make(map[int]int)

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			k := rand.Intn(10) + 1  // from 1 to 10
			v := rand.Intn(100) + 1 // from 1 to 100
			mp[k] = v
		}()
	}

	wg.Wait()
}
