package main

import (
	"fmt"
	"sync"
)

type Config struct {
	State string
}

var (
	cfg        Config
	onceConfig sync.Once
)

func initConfig() {
	cfg = Config{State: "init"}
}

func GetConfig() *Config {
	onceConfig.Do(initConfig)
	return &cfg
}

func main() {
	config := GetConfig()
	fmt.Printf("config: %s\n", config.State)

	config.State = "updated"
	fmt.Printf("config: %s\n", config.State)

	newConfig := GetConfig()
	fmt.Printf("newConfig: %s\n", newConfig.State)
}
