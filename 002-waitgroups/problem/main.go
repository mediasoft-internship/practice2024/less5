package main

import (
	"fmt"
	"math/rand"
	"time"
)

func task(i int) {
	time.Sleep(time.Duration(rand.Intn(5+1-2)+2) * time.Second) // sleep from 2 to 5 seconds
	fmt.Printf("success %d\n", i)
}

func main() {
	tNow := time.Now()

	for i := 0; i < 10; i++ {
		go task(i)
	}

	fmt.Printf("execution time: %v\n", time.Since(tNow))
}
