package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func task(wg *sync.WaitGroup, i int) {
	time.Sleep(time.Duration(rand.Intn(5)+1) * time.Second) // sleep from 1 to 5 seconds
	fmt.Printf("success %d\n", i)
	wg.Done()
}

func main() {
	wg := sync.WaitGroup{}
	tNow := time.Now()

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go task(&wg, i)
	}

	wg.Wait()
	fmt.Printf("execution time: %v\n", time.Since(tNow))
}
