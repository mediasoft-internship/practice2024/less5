package main_test

import (
	"sync"
	"sync/atomic"
	"testing"
)

/*
	for win
	go test -bench . -benchmem

	for linux/darwin
	go test -bench=. -benchmem
*/

func BenchmarkAtomic(b *testing.B) {
	var counter int32
	for i := 0; i < b.N; i++ {
		atomic.AddInt32(&counter, 1)
	}
}

func BenchmarkMutex(b *testing.B) {
	mu := sync.Mutex{}

	var counter int32
	for i := 0; i < b.N; i++ {
		mu.Lock()
		counter++
		mu.Unlock()
	}
}
