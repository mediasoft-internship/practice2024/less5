package main

import (
	"sync"
	"testing"
	"unsafe"
)

func BenchmarkWithoutPool(b *testing.B) {
	wg := sync.WaitGroup{}
	mp := sync.Map{}

	for i := 0; i < b.N; i++ {
		wg.Add(1)

		go func(i int) {
			value := new(int)
			*value = i

			mp.Store(value, struct{}{})
			wg.Done()
		}(i)
	}

	wg.Wait()

	var totalMem int
	mp.Range(func(value, _ any) bool {
		totalMem += int(unsafe.Sizeof(*value.(*int)))
		return true
	})
	b.Logf("total used memory: %d bytes", totalMem)
}

func BenchmarkWithPool(b *testing.B) {
	wg := sync.WaitGroup{}
	bytesPool := sync.Pool{
		New: func() any {
			return new(int)
		},
	}
	mp := sync.Map{}

	for i := 0; i < b.N; i++ {
		wg.Add(1)

		go func(i int) {
			value := bytesPool.Get().(*int)
			*value = i
			bytesPool.Put(value)

			mp.Store(value, struct{}{})
			wg.Done()
		}(i)
	}

	wg.Wait()

	var totalMem int
	mp.Range(func(value, _ any) bool {
		totalMem += int(unsafe.Sizeof(*value.(*int)))
		return true
	})
	b.Logf("total used memory: %d bytes", totalMem)
}
