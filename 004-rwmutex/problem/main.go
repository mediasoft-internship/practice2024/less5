package main

import (
	"fmt"
	"sync"
	"time"
)

type Data struct {
	data int
	mu   sync.Mutex
}

func NewData() *Data {
	return new(Data)
}

func (m *Data) Get() int {
	m.mu.Lock()
	defer m.mu.Unlock()
	return m.get()
}

func (m *Data) get() int {
	time.Sleep(1 * time.Second)
	return m.data
}

func main() {
	wg := sync.WaitGroup{}
	sMap := NewData()
	tNow := time.Now()

	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func() {
			sMap.Get()
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Printf("execution time: %v\n", time.Since(tNow))
}
